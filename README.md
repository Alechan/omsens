# Deprecated
Old repo of OMSens plugin of OpenModelica. Moved to https://github.com/OpenModelica/OMSens.

# Reqs

- Linux
- Python 3 with some extra libraries. (anaconda3 includes all the dependencies)
- OpenModelica basic install (with "omc" command in path in Linux or "%OPENMODELICAHOME%\\bin\\omc" for Windows). The SystemDynamics library is included in this repo and the one included in the complete install is not needed (or used)